module.exports = function (sequelize, DataTypes) {
    var UserValidation = sequelize.define('UserValidation', {
        email: {type: DataTypes.STRING},
        uid: {type: DataTypes.STRING(64)},
        email_code: DataTypes.STRING,
        email_verified: DataTypes.BOOLEAN,
        phone: DataTypes.STRING,
        country: DataTypes.STRING,
        phone_code: DataTypes.STRING,
        phone_verified: DataTypes.BOOLEAN,
        remote_ip: DataTypes.STRING,
    }, {
        tableName: 'user_validation',
        createdAt   : 'created_at',
        updatedAt   : 'updated_at',
        timestamps  : true,
        underscored : true,
    });
    return UserValidation;
};