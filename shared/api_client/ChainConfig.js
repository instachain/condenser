import * as golos from 'golos-js';

golos.config.set('address_prefix','INS');

let chain_id = "e9612c0b8ec8206dfcd10a12bf28be747d6b62d536306df0d68978993d13c823"
// for(let i = 0; i < 32; i++) chain_id += "00"

module.exports = {
    address_prefix: "INS",
    expire_in_secs: 15,
    chain_id
}
