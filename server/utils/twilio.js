import Twilio from 'twilio';
import config from 'config';

const accountSid = config.get('twilio.account_sid');
const authToken = config.get('twilio.auth_token');
const senderId = "+1727-233-4833";
const client = Twilio(accountSid, authToken);

function checkEligibility(phone) {
    // US, Canada +1
    // France +33
    // Spain +34
    // Italy +39
    // UK +44
    // Sweden +46
    // Germany +49
    // Mexico +52
    // Australia +61
    // Phillipines +63
    // Singapore +65
    // Turkey +90
    // Hong Kong +852
    // Israel +972

    for(const prefix of ['1', '33', '34', '39', '44', '46', '49', '52', '61', '63', '65', '90', '852', '972']) {
        if (phone.startsWith(prefix)) return true;
    }
    return false;
}

export default function sendVerifySMS(phone, confirmation_code) {

  // var accountSid = 'AC494b8cb4c88168b14b995d6e614095e4';
  // var authToken = 'c961107ef0f850c03eaae15dc4557d56';

  console.log('*********** sendVerifySMS : *********** ', phone, confirmation_code);
  var body = "Thank you for joining INSTACHAIN. This is your confirmation code: "+confirmation_code;

  client.messages.create({
      from: senderId,
      to: '+' + phone.toString(),
      body: body

  }, function(err, response) {
      if (err) {
          console.log('*********** send Sms Message ERROR : *********** ', err);
      } else {
          console.log('*********** send Sms Message Success : *********** ', response);
      }
  });
}
